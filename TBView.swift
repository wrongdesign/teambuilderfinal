//
//  TBView.swift
//  TeamBuilde3r
//
//  Created by Mac-mini-1 on 12/11/19.
//  Copyright © 2019 student 2. All rights reserved.
//

import UIKit

@IBDesignable
class TBView: UIView {

    @IBOutlet weak var coeffLabel: UILabel!
    @IBOutlet weak var coeffButton: UISwitch!
    
    @IBInspectable
    var lText: String? {
        get {
            return self.coeffLabel.text
        }
        set {
            self.coeffLabel.text = newValue
        }
    }
    
    @IBInspectable
    var toggle: Bool {
        get {
            return self.coeffButton.isHidden
        }
        set {
            self.coeffButton.isHidden = newValue
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        loadFromNib()
    }
}
