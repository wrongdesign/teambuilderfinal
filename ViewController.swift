//
//  ViewController.swift
//  TeamBuilde3r
//
//  Created by student 2 on 12/10/19.
//  Copyright © 2019 student 2. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var ageAndCity: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var aboutPerson: UITextView!
    @IBOutlet weak var secondLine: UIView!
    @IBOutlet weak var firstLine: UIView!
}

