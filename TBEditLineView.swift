//
//  TBEditLineView.swift
//  TeamBuilde3r
//
//  Created by Mac-mini-1 on 12/11/19.
//  Copyright © 2019 student 2. All rights reserved.
//

import UIKit

@IBDesignable
class TBEditLineView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var lineLabel: UIView!
    
    @IBInspectable
    var titleText: String? {
        get {
            return self.titleLabel.text
        }
        set {
            self.titleLabel.text = newValue
        }
    }
    
    @IBInspectable
    var descroptionText: String? {
        get {
            return self.descriptionLabel.text
        }
        set {
            self.descriptionLabel.text = newValue
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        loadFromNib()
    }
}
