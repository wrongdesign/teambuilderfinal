//
//  TBEditLineWithArrowView.swift
//  TeamBuilde3r
//
//  Created by Mac-mini-1 on 12/11/19.
//  Copyright © 2019 student 2. All rights reserved.
//

import UIKit

@IBDesignable
class TBEditLineWithArrowView: UIView {

    @IBOutlet weak var titLabel: UILabel!
    @IBOutlet weak var descriptLabel: UILabel!
    @IBOutlet weak var arrayImage: UIImageView!
    
    @IBInspectable
    var titText: String? {
        get {
            return self.titLabel.text
        }
        set {
            self.titLabel.text = newValue
        }
    }
    
    @IBInspectable
    var descroptText: String? {
        get {
            return self.descriptLabel.text
        }
        set {
            self.descriptLabel.text = newValue
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        loadFromNib()
    }

}
