//
//  TBRateView.swift
//  TeamBuilde3r
//
//  Created by Mac-mini-1 on 12/11/19.
//  Copyright © 2019 student 2. All rights reserved.
//

import UIKit

@IBDesignable
class TBRateView: UIView {

    @IBOutlet weak var lLabel: UILabel!
    @IBOutlet weak var switchButton: UISlider!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    
    @IBInspectable
    var lText: String? {
        get {
            return self.lLabel.text
        }
        set {
            self.lLabel.text = newValue
        }
    }
    
    @IBInspectable
    var toggle: Bool {
        get {
            return self.switchButton.isHidden
        }
        set {
            self.switchButton.isHidden = newValue
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        loadFromNib()
    }

}
