//
//  TBArrowView.swift
//  TeamBuilde3r
//
//  Created by Mac-mini-1 on 12/11/19.
//  Copyright © 2019 student 2. All rights reserved.
//

import UIKit


@IBDesignable
class TBArrowView: UIView {

    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var lineLabel: UIView!
    
    @IBInspectable
    var leftText: String? {
        get {
            return self.leftLabel.text
        }
        set {
            self.leftLabel.text = newValue
        }
    }
    
    @IBInspectable
    var rightText: String? {
        get {
            return self.rightLabel.text
        }
        set {
            self.rightLabel.text = newValue
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        loadFromNib()
    }
}
